<?php
// Modeline for drupal
// vim: set expandtab tabstop=2 shiftwidth=2 autoindent smartindent filetype=php:

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * ZINA (Zina is not Andromeda)
 *
 * Zina is a graphical interface to your MP3 collection, a personal
 * jukebox, an MP3 streamer. It can run on its own, embeded into an
 * existing website, or as a Drupal/Joomla/Wordpress/etc. module.
 *
 * http://www.pancake.org/zina
 * Author: Ryan Lathouwers <ryanlath@pacbell.net>
 * Support: http://sourceforge.net/projects/zina/
 * License: GNU GPL2 <http://www.gnu.org/copyleft/gpl.html>
 *
 * Drupal Module:
 *  - Zina testbed module...should be most complete.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
define('__ZINA__', 'music');

/**
 * Implementation of hook_help().
 */
function zina_help($section) {
  switch ($section) {
    case 'admin/help#zina':
      return t("XXX");
  }
}

/**
 * Implementation of hook_perm().
 */
function zina_perm() {
  return array('administer zina', 'access zina' ,'create zina playlists', 'zina editor');
}

/**
 * Implementation of hook_menu().
 */
function zina_menu() {
  $items['admin/settings/zina'] = array(
    'title' => t('Zina'),
    'description' => t('Zina module settings.'),
    'page callback' => 'zina_admin_settings',
    'access arguments' => array('administer zina'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items[__ZINA__] = array(
    'title' => t('Music'),
    'description' => t('Zina is a graphical interface to your MP3 collection, a personal jukebox, an MP3 streamer'),
    'page callback' => 'zina_main',
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

function zina_get_active_db(&$opts) {
  global $active_db, $db_type, $db_prefix;
  $opts['prefix'] = $db_prefix;
  $opts['type'] = $db_type;
  return $active_db;
}

function zina_main() {
  global $user;
  require_once('zina/index.php');
  $conf['time'] = microtime(true);

  if (isset($_GET['zid']) && isset($_GET['l']) && ($_GET['l'] == '6' || $_GET['l'] == '10')) {
    $access = false;
    if ($uid = zina_token('verify',$_GET['zid'])) {
      $user = db_fetch_object(db_query("SELECT u.*, s.* FROM {users} u INNER JOIN {sessions} s ON u.uid = s.uid WHERE u.uid = '%s'", $uid));

      // We found the client's session record and they are an authenticated user
      if ($user && $user->uid > 0 && user_access('access zina', user_load($user->uid))) {
        sess_destroy_sid(session_id());
        session_id($user->sid);
        $access = true;
        $user = drupal_unpack($user);
      }
    }
    if (!$access) { 
      drupal_access_denied(); 
      return; 
    }
  } elseif (!user_access('access zina')) { 
    drupal_access_denied(); 
    return; 
  }

  $conf['embed'] = 'drupal';
  $conf['is_admin'] = user_access('administer zina');
  $conf['index_abs'] = dirname(__FILE__);

  if ($clean_urls = (bool)variable_get('clean_url', '0')) {
    $conf['clean_urls_hack'] = false;
    $conf['index_rel'] = __ZINA__;
    $conf['clean_urls'] = true;
  } else {
    $conf['url_query'][] = 'q='.__ZINA__;
    $conf['clean_urls'] = false;
  }

  $conf['user_id'] = $user->uid;

  if ($user->uid > 0) {
    if ($user->lastfm) {
      $conf['lastfm'] = true;
      $conf['lastfm_username'] = $user->lastfm_username;
      $conf['lastfm_password'] = $user->lastfm_password;
    }
    if ($user->twitter) {
      $conf['twitter'] = true;
      $conf['twitter_username'] = $user->twitter_username;
      $conf['twitter_password'] = $user->twitter_password;
    }
  }

  if (!isset($_GET['p'])) {
    $_GET['p'] = substr($_GET['q'],strlen(__ZINA__)+1);
  }
  
  $conf['site_name'] = variable_get('site_name', '');

  $zina = zina($conf);

  global $zina_cms_teaser;
  if (!$zina_cms_teaser) {
    $bc = drupal_get_breadcrumb();
    if (is_array($zina['breadcrumb'])) $bc = array_merge($bc, $zina['breadcrumb']);
    drupal_set_breadcrumb($bc);

    # todo potentially overirde _ZARTIST? w/ drupal_get_title()?
    #drupal_set_title( drupal_get_title(). ': ' . $zina['title'] );
    drupal_set_title($zina['title']);
  }
  drupal_set_html_head($zina['head_html'].$zina['head_css']);
  $js = zina_set_js();
  foreach($js['file'] as $file=>$nothing) {
    if ($file == 'extras/jquery.js' || $file == 'extras/drupal.js') continue;
    drupal_add_js(drupal_get_path('module', 'zina').'/zina/'.$file, 'module');
  }
  if (!empty($js['vars'])) drupal_add_js(implode('',$js['vars']), 'inline');
  if (!empty($js['jquery'])) drupal_add_js('jQuery().ready(function(){'.implode('', $js['jquery']).'});', 'inline');
  if (!empty($js['inline'])) drupal_add_js(implode('',$js['inline']), 'inline');

  if ($zina) {
    global $zina_nid;
    if (!empty($zina_nid)) {
      # drupal paths
      return $zina;
    } elseif (isset($zina['page_type']) && $zina['page_type'] == 'main') {
      # zina paths
      $node_id = zdbq_single("SELECT cms_id FROM {dirs} WHERE path = '%s'", array($zina['path']));
      if (!empty($node_id)) {
        $_GET['q'] = 'node/'.$node_id;
        $node = node_load($node_id);
        $node->zina['complete'] = ztheme('page_complete', $zina); 
        return node_show($node, null);
        #return node_view($node, false, true);
      }
    }
    return ztheme('page_complete', $zina);
  }
}

function zina_admin_settings() {
  drupal_goto(__ZINA__.'/','l=20');
  return;
}

/**
 * Implementation of hook_node_info
 */
function zina_node_info() {
  return array(
    'zina' => array(
      'name' => t('Zina'),
      'module' => 'zina',
      'description' => t('Zina internal node.'),
      'has_body' => 0,
      'has_title' => 0,
    )
  );
}

# needed
function zina_form(&$node) {
 /* 
  $form = array();
  $type = node_get_types('type', $node);
  if ($type->has_title) {
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => check_plain($type->title_label),
      '#required' => TRUE,
      '#default_value' => $node->title,
      '#weight' => -5
    );
  }
  
  return $form;
  */
}

function zina_cms_insert($path) {
	$crumbs = explode('/',$path);
  foreach($crumbs as $crumb) {
		$titles[] = ztheme('title',zcheck_utf8($crumb));
	}
  
  $node->type = 'zina';
  $node->title = implode('', $titles);
  $node->uid = variable_get('zina_drupal_uid', 1);
  $node->promote = 0;
  node_save($node);
  return $node->nid;
}

function zina_cms_select($cms_id) {
  return node_load($cms_id);
}

function zina_cms_alias($cms_id, $path) {
  static $account = array();
  if (empty($account)) {
    $uid = variable_get('zina_drupal_uid', 1);
    $account = user_load($uid);
  }
  if (!user_access('create url aliases', $account)) return false;

  $parts = explode('/', $path);
  foreach($parts as $part) {
    $paths[] = zcheck_plain($part);
  }
  if (!isset($paths)) {
    $safe_path = zt('Artists');
  } else {
    $safe_path = implode('/',$paths);
  }

  # GOOD: /music/Bj%F6rk
  # BAD:  /music/Bj%C3%B6rk
  return path_set_alias('node/'. $cms_id, __ZINA__.'/'.$safe_path);
}

function zina_cms_tags($cms_id, $genre) {
  $node->nid = $cms_id;
  $node->vid = $cms_id;
  $node->type = 'zina';
  $node->uid = variable_get('zina_drupal_uid', 1);
  $node->taxonomy['tags'][] = $genre;
  $terms = taxonomy_node_get_terms($node);
  
  if (empty($terms)) {
    $vid = variable_get('zina_genre_vocabulary', 0);
    $term = array(
      'tags' => array($vid => $genre),
    );
    taxonomy_node_save($node, $term);
  }
}

function zina_delete(&$node) {
  db_query('UPDATE {zina_dirs} SET cms_id = 0 WHERE cms_id = %d', $node->nid);
}

function zina_load(&$node) {
    global $zina_nid;
    $zina_nid = $node->nid;
    #return $additions;
}

function zina_view($node, $teaser = FALSE, $page = FALSE) {
  if (isset($node->zina['complete'])) {
    # zina paths
    $node->content['zina'] = array(
      '#value' => $node->zina['complete'],
      '#weight' => 0, #variable_get('zina
    );
  } else {
    # drupal paths
    $zina_path = db_result(db_query("SELECT path FROM {zina_dirs} WHERE cms_id = %d", array($node->nid)));
    if (!empty($zina_path)) {
      global $zina_cms_teaser;
      $zina_cms_teaser = $teaser;
      $_GET['p'] = utf8_decode($zina_path);

      $zina = zina_main();
      $node->zina = &$zina;
      $node->title = theme('zina_title', $zina['title']);
      if ($teaser) {
        $node->readmore = true;
        $node->teaser = $content = theme('zina_teaser', $zina);
      } else {
        $content = ztheme('page_complete', $zina); 
      }  
    } else {
      $content = t('Error');
    }
    
    $node->content['zina'] = array(
      '#value' => $content,
      '#weight' => 0, #variable_get('zina
    );
  }
  return $node;
}

#function zina_insert($node) { }

/**
 * Implementation of hook_nodeapi().
 */
function zina_nodeapi(&$node, $op, $teaser, $page) { 
  switch ($op) {
    case 'delete':
      break;

    case 'load':
      break;

    case 'update':
      break;
      
    case 'insert':
      break;

    case 'presave':
      break;

    case 'view':
      break;

    case 'search result':
      break;

    case 'update index':
      break;
  }
}

/**
 * Implementation of hook_block().
 */
function zina_block($op = 'list', $delta = null, $edit = array()) {
  if (!function_exists('zina_init')) {
    $conf['time'] = microtime(true);
    $conf['embed'] = 'drupal';
    $conf['index_abs'] = dirname(__FILE__);
    if ($clean_urls = (bool)variable_get('clean_url', '0')) {
      $conf['clean_urls_hack'] = false;
      $conf['index_rel'] = __ZINA__;
      $conf['clean_urls'] = true;
    } else {
      $conf['url_query'][] = 'q='.__ZINA__;
      $conf['clean_urls'] = false;
    }
    
    require_once('zina/index.php');
    zina_init($conf);
  }

  if (!zina_get_setting('database')) return;

  if ($op == 'list') {
    #todo: make 5 configable
    for ($i=1; $i<=5; $i++) {
      $blocks['zina_block_'.$i]['info'] = t('Zina Block @num', array('@num'=>$i));
    }
     $blocks['zina_block_zinamp']['info'] = t('Zina Flash Player');
    return $blocks;

  } elseif ($op == 'view') {
    if ($delta == 'zina_block_zinamp') {
      if ($_GET['q'] != __ZINA__) {
        $block['subject'] = $blocks[$delta]['info'];
        $block['content'] = ztheme('zinamp_embed');
        #TODO: XXX redudant
        $js = zina_set_js();
        foreach($js['file'] as $file=>$nothing) {
          if ($file == 'extras/jquery.js' || $file == 'extras/drupal.js') continue;
          drupal_add_js(drupal_get_path('module', 'zina').'/zina/'.$file, 'module');
        }
        if (!empty($js['vars'])) drupal_add_js(implode('',$js['vars']), 'inline');
        if (!empty($js['jquery'])) drupal_add_js('jQuery().ready(function(){'.implode('', $js['jquery']).'});', 'inline');
        if (!empty($js['inline'])) drupal_add_js(implode('',$js['inline']), 'inline');

        return $block;
      }
    } else {
      $number = (int) variable_get($delta.'_items', 5);
      $type = variable_get($delta.'_type', 'f');
      $page = variable_get($delta.'_page', 'rating');
      $period = variable_get($delta.'_period', 'all');
      $id3 = variable_get($delta.'_id3', 0);
      $images = variable_get($delta.'_images', 1);
      $results = zina_get_block_stat($type, $page, $period, $number, $id3, $images);
      if (sizeof($results) > $number) $results = array_slice($results,0, $number);
      $block['subject'] = $blocks[$delta]['info'];
      $block['content'] = theme('zina_block', $results, array('type'=>$type, 'page'=>$page, 'period'=>$period, 'images'=>$images));
      return $block;
    }
  } elseif ($op == 'configure') {
    if ($delta == 'zina_block_zinamp') {

      #return $form;
    } else {
      $types = zina_get_stats_types();
      $form[$delta.'_type'] = array(
        '#type' => 'select',
        '#title' => t('Type of Item'),
        '#options' => $types,
        '#default_value' => variable_get($delta.'_type', 'song'),
        '#required' => true,
      );

      #$pages = zina_get_stats_pages();
      $pages = zina_get_stats_blocks();
      $form[$delta.'_page'] = array(
        '#type' => 'select',
        '#title' => t('Type of statistic'),
        '#options' => $pages,
        '#default_value' => variable_get($delta.'_page', 'rating'),
        '#required' => true,
      );

      $periods = zina_get_stats_periods();
      $form[$delta.'_period'] = array(
        '#type' => 'select',
        '#title' => t('Period of statistic'),
        '#description' => t('Will not be relevant for all stats'),
        '#options' => $periods,
        '#default_value' => variable_get($delta.'_period', 'all'),
        '#required' => true,
      );

      $form[$delta.'_items'] = array(
        '#type' => 'textfield',
        '#title' => t('Number of items'),
        '#description' => t('This needs to be a number.'),
        '#default_value' => variable_get($delta.'_items', 10),
        '#required' => true,
      );
      $form[$delta.'_id3'] = array(
        '#type' => 'radios',
        '#title' => t('Use id3 tags (if applicable)'),
        '#options' => array(1=> t('True'), 0=>t('False')),
        '#default_value' => variable_get($delta.'_id3', 0),
        '#required' => true,
      );
      $form[$delta.'_images'] = array(
        '#type' => 'radios',
        '#title' => t('Show images'),
        '#options' => array(1=> t('True'), 0=>t('False')),
        '#default_value' => variable_get($delta.'_images', 1),
        '#required' => true,
      );
      return $form;
    }
  } elseif ($op == 'save') {
    if ($delta == 'zina_block_zinamp') {

    } else {
      $options = array('items','type', 'page', 'period', 'id3', 'images');
      foreach ($options as $opt) {
        variable_set($delta.'_'.$opt, $edit[$delta.'_'.$opt]);
      }
    }
  }
}

/*function zina_link($type, $object, $teaser = false) {
  $links = array();
  if ($type == 'node' && $object->type == 'zina') {
    if ($teaser) { 
      $links['zina_read_more'] = array(
        'title' => t('Read More'),
        'href' => 'asdf',
        'attributes' => array('title' => t('Read the rest of !title.', array('!title' => $node->title)))
      );
    }
  }
  return $links;
}
 */
/**
 * Implementation of hook_theme().
 */
function zina_theme() {
  return array(
    'zina_admin_view' => array(
      'arguments' => array('form'),
    ),
    'zina_block' => array(
      'arguments' => array('items','opts'),
    ),
    'zina_teaser' => array(
      'arguments' => array('zina'),
    ),
    'zina_title' => array(
      'arguments' => array('path'),
    ),
  );
}

function theme_zina_title($title) {
  return decode_entities($title);
}

function theme_zina_teaser($zina) {
  $output = '<div class="node-content clear-block"><div class="zina-directory-image">'.zl($zina['dir_image_sub'], $zina['path']).'</div>';
  if (!empty($zina['description'])) {
    $output .= '<p>'.$zina['description'].'</p>';
  }
  $output .= '</div>';
  return $output;
}

function zina_cron() {
  $conf['time'] = microtime(true);
  $conf['embed'] = 'drupal';
  $conf['index_abs'] = dirname(__FILE__);
  require_once('zina/index.php');
  zina_init($conf);
  zina_cron_run();
}


function theme_zina_block($items, $opts) {
  if (!empty($items)) {

    if ($opts['images']) {
      if ($_GET['q'] != __ZINA__) {
        drupal_set_html_head('<style>div.zina-stats-block{float:left;width:100%;}'.
			  '.zina-stats-block p{margin:0;padding:0;}'.
			  '.zina-stats-block img{float:left;padding-right:5px;padding-bottom:5px;}'.
			  '</style>');
      }
      foreach($items as $item) {
        $output .= '<div class="zina-stats-block">'.
          '<a href="'.$item['image_url'].'">'.$item['image'].'</a>'.
          '<p>'.$item['display'].'</p>'.
          '</div>';
      }
    } else {

      $output = '<ul class="item-list">';
      foreach($items as $item) {
        $output .= '<li>'.$item['display'].'</li>';
      }
      $output .= '</ul>';
    }
  } else {
    $output = t('No results');
  }
  return $output;
}

function zina_user($op, &$edit, &$account, $category = NULL) {
  if ($op == 'form' && $category == 'account') {
    $form['zina'] = array(
      '#type' => 'fieldset',
      '#title' => t('Last.fm and/or Twitter Integration'),
      '#description' => t('If you want to send your listening information to last.fm or twitter.com enable this and enter your login credentials.'),
      '#collapsible' => TRUE,
      '#weight' => 4);
    $form['zina']['lastfm'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable last.fm integration'),
      '#default_value' => $edit['lastfm'],
    );
    $form['zina']['lastfm_username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => $edit['lastfm_username'],
      '#size' => 20,
    );
    $form['zina']['lastfm_password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => $edit['lastfm_password'],
      '#size' => 20,
    );
    $form['zina']['twitter'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable twitter integration'),
      '#default_value' => $edit['twitter'],
    );
    $form['zina']['twitter_username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => $edit['twitter_username'],
      '#size' => 20,
    );
    $form['zina']['twitter_password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => $edit['twitter_password'],
      '#size' => 20,
    );
    return $form;
  }
}

function zina_access_denied() {
  drupal_access_denied();
  exit;
}

function zina_not_found() {
  drupal_not_found();
  return false;
}

function zina_cms_info() {
  return array(
    'version' => VERSION,
    'modules' => module_list(false, false),
  );
}

function zina_cms_user($user_id = false) {
  global $user;
  static $users;

  $uid = ($user_id !== false) ? $user_id : $user->uid;

  if (isset($users[$uid])) return $users[$uid];

  $user_local = ($user_id) ? user_load($uid) : $user;

  if ($uid == 0) {
    $users[$uid] = false;
  } else {
    $users[$uid] = array(
      'uid' => $user_local->uid,
      'name' => $user_local->name,
      'profile_url' => url('user/'.$user_local->uid),
    );
  }
  return $users[$uid];
}

function zina_token($type, $value) {
	$sitekey = drupal_get_private_key();

	$sep = '|';
	if ($type == 'get') {
		return $value.$sep.md5($value.$sitekey);
	} elseif ($type == 'verify') {
		$x = explode($sep, $value);
		if (md5($x[0].$sitekey) === $x[1]) {
			return $x[0];
		} else {
			return false;
		}
	}

	return false;
}

function zina_xmlsitemap_links($type = null, $excludes = array()) {
  if ($type == 'xml') {
    $conf['time'] = microtime(true);
    $conf['embed'] = 'drupal';
    $conf['index_abs'] = dirname(__FILE__);
    if ($clean_urls = (bool)variable_get('clean_url', '0')) {
      $conf['clean_urls_hack'] = false;
      $conf['index_rel'] = __ZINA__;
      $conf['clean_urls'] = true;
    } else {
      $conf['url_query'][] = 'q='.__ZINA__;
      $conf['clean_urls'] = false;
    }
    
    require_once('zina/index.php');
    zina_init($conf);
    return zina_content_sitemap();
  }
}

/*
function zina_search_page($results) {
  #TODO: theme zina info
  return theme('search_results', $results, 'zina');
}
 */

#TODO: test out on big search (limit? make opt?)
function zina_search($op = 'search', $keys = null) {
  if ($op == 'name') {
    return t('Music');
  } elseif ($op == 'search') {
    $count = variable_get('minimum_word_size', 3);
    if (empty($keys) || strlen($keys) < $count) {
      form_set_error('keys', t('You must include at least one keyword with @count characters or more.', array('@count' => $count)));
      return;
    }

    global $user;
    if (!user_access('access zina', $user)) return;

      if (!function_exists('zina_init')) {
        $conf['time'] = microtime(true);
        $conf['embed'] = 'drupal';
        $conf['index_abs'] = dirname(__FILE__);
        if ($clean_urls = (bool)variable_get('clean_url', '0')) {
          $conf['clean_urls_hack'] = false;
          $conf['index_rel'] = __ZINA__;
          $conf['clean_urls'] = true;
        } else {
          $conf['url_query'][] = 'q='.__ZINA__;
          $conf['clean_urls'] = false;
        }
    
        require_once('zina/index.php');
        zina_init($conf);
      }

      $results = zina_search_embed($keys);

      foreach($results as $item) {
        $drupal_results[] = array(
          'link' => zurl($item['title_link']['path'],str_replace('&amp;','&',$item['title_link']['query'])),
          #'type' => ucfirst($item['type']),
          'title' => $item['title'],
          'snippet' => $item['description'],
        );
      }
      return $drupal_results;

/*
    $results[] = array(
      'link' => url('node/'. $item->sid, array('absolute' => TRUE)),
      'type' => check_plain(node_get_types('name', $node)),
      'title' => $node->title,
      'user' => theme('username', $node),
      'date' => $node->changed,
      'node' => $node,
      'extra' => $extra,
      'score' => $item->score / $total,
      'snippet' => search_excerpt($keys, $node->body),
    );
 */
  }
}

function zina_update_index() {
  $limit = (int)variable_get('search_cron_limit', 50);
  $result = db_query_range("SELECT n.nid FROM {node} n LEFT JOIN {search_dataset} d ON d.type = 'zina' AND d.sid = n.nid ".
    "WHERE d.sid IS NULL OR d.reindex <> 0 ORDER BY d.reindex ASC, n.nid ASC", 0, $limit);

  while ($node = db_fetch_object($result)) {
    $node = node_load($node->nid);
    
    if (node_hook($node, 'view')) {
      node_invoke($node, 'view', false, false);
    } else {
      $node = node_prepare($node, false);
    }

    // Allow modules to change $node->body before viewing.
    node_invoke_nodeapi($node, 'view', false, false);

    $text = '<h1>'. check_plain($node->title) .'</h1>'. $node->body;

    // Fetch extra data normally not visible
    $extra = node_invoke_nodeapi($node, 'update index');
    foreach ($extra as $t) {
      $text .= $t;
    }

    // Update index
    search_index($node->nid, 'node', $text);
  }
}

function zina_cms_access($type, $type_user_id = null) {
  global $zc, $user;

  if ($zc['is_admin']) return true;

  switch ($type) {
    case 'edit_playlists':
      return ($zc['pls_user'] && user_access('create zina playlists', $user) && $zc['user_id'] == $type_user_id);
    case 'editor':
      return user_access('zina editor', $user);
  }

  return false;
}
?>
